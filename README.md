# Teste Landing Estagiário #

### O que fazer? 
* Criar landing de acordo com o demonstrado no PDF
* As imagens necessárias estão na pasta img
* Usar este repositório para organizar o projeto
* Usar um framework CSS (Usamos materialize, mas pode escolher outro de preferência)
* Criar animação da parte principais benefícios
* Criar animação de scroll no topo da página para a section logo em seguida.

### Cores:
* Principal: #C90B5C

### Fontes:
* Principal: https://fonts.google.com/specimen/Montserrat

### Entregar 29/10/2020
